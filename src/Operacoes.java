public class Operacoes {

    public double operacao(double n1, double n2) {
        double retorno;
        int operacoes

        switch (operacoes) {
            case 1:
                Soma som = new Soma();
                retorno = som.soma(n1, n2);
                break;
            case 2:
                Subtracao sub = new Subtracao();
                retorno = sub.subtracao(n1, n2);
                break;
            case 3:
                Divisao div = new Divisao();
                retorno = div.divisao(n1, n2);
                break;
            case 4:
                Multiplicacao multi = new Multiplicacao();
                retorno = multi.multiplicacao(n1, n2);
                break;
            default:
                retorno = -1;
        }

        return retorno;
    }
}
